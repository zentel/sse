#include "test_sse_gen.inl"
#include <assert.h>
#include <Windows.h>
#include <stdio.h>

void test() {
	char buf[32];
	const char* str = "hello world!";
	sse_str_t ss = sse_str_new(str);

	unsigned int hash1 = sse_str_hash(ss, 216613626UL);
	unsigned int hash2 = sse_hash(ss, 216613626UL);
	assert(hash1 == hash2);
	assert(sse_str_len(ss) == strlen(str));
	sse_str_free(ss);

	ss = sse_str_init_buffer(buf, sizeof(buf), str);
	assert(ss);

}


int toNumber1(const char* str) {
	SSE_SWITCH(str) {
		SSE_CASE(one) :
			return 1;
		SSE_CASE(two) :
			return 2;
		SSE_CASE(three) :
			return 3;
		SSE_CASE(four) :
			return 4;
		SSE_CASE(five) :
			return 5;
		SSE_CASE(six) :
			return 6;
		SSE_CASE(seven) :
			return 7;
		SSE_CASE(eight) :
			return 8;
		SSE_CASE(nine) :
			return 9;
		SSE_CASE(ten) :
			return 10;
		SSE_CASE(eleven) :
			return 11;
		SSE_CASE(twelve) :
			return 12;
		SSE_CASE(thirteen) :
			return 13;
		SSE_CASE(fourteen) :
			return 14;
		SSE_CASE(fifteen) :
			return 15;
		SSE_CASE(sixteen) :
			return 16;
		SSE_CASE(seventeen) :
			return 17;
		SSE_CASE(eighteen) :
			return 18;
		SSE_CASE(nineteen) :
			return 19;
		SSE_CASE(twenty) :
			return 20;
		default:
			return 0;
	}
}

int toNumber2(sse_str_t str) {
	SSE_SWITCH_SS(str) {
		SSE_CASE(one) :
			return 1;
		SSE_CASE(two) :
			return 2;
		SSE_CASE(three) :
			return 3;
		SSE_CASE(four) :
			return 4;
		SSE_CASE(five) :
			return 5;
		SSE_CASE(six) :
			return 6;
		SSE_CASE(seven) :
			return 7;
		SSE_CASE(eight) :
			return 8;
		SSE_CASE(nine) :
			return 9;
		SSE_CASE(ten) :
			return 10;
		SSE_CASE(eleven) :
			return 11;
		SSE_CASE(twelve) :
			return 12;
		SSE_CASE(thirteen) :
			return 13;
		SSE_CASE(fourteen) :
			return 14;
		SSE_CASE(fifteen) :
			return 15;
		SSE_CASE(sixteen) :
			return 16;
		SSE_CASE(seventeen) :
			return 17;
		SSE_CASE(eighteen) :
			return 18;
		SSE_CASE(nineteen) :
			return 19;
		SSE_CASE(twenty) :
			return 20;
		default:
			return 0;
	}
}


int toNumber(const char* str) {
	if (strcmp(str, "one") == 0) {
		return 1;
	}
	else if (strcmp(str, "two") == 0) {
		return 2;
	}
	else if (strcmp(str, "three") == 0) {
		return 3;
	}
	else if (strcmp(str, "four") == 0) {
		return 4;
	}
	else if (strcmp(str, "five") == 0) {
		return 5;
	}
	else if (strcmp(str, "six") == 0) {
		return 6;
	}
	else if (strcmp(str, "seven") == 0) {
		return 7;
	}
	else if (strcmp(str, "eight") == 0) {
		return 8;
	}
	else if (strcmp(str, "nine") == 0) {
		return 9;
	}
	else if (strcmp(str, "ten") == 0) {
		return 10;
	}
	else if (strcmp(str, "eleven") == 0) {
		return 11;
	}
	else if (strcmp(str, "twelve") == 0) {
		return 12;
	}
	else if (strcmp(str, "thirteen") == 0) {
		return 13;
	}
	else if (strcmp(str, "fourteen") == 0) {
		return 14;
	}
	else if (strcmp(str, "fifteen") == 0) {
		return 15;
	}
	else if (strcmp(str, "sixteen") == 0) {
		return 16;
	}
	else if (strcmp(str, "seventeen") == 0) {
		return 17;
	}
	else if (strcmp(str, "eighteen") == 0) {
		return 18;
	}
	else if (strcmp(str, "nineteen") == 0) {
		return 19;
	}
	else if (strcmp(str, "twenty") == 0) {
		return 20;
	}
	else {
		return 0;
	}
}

int main(int argc, char** argv) {
	//test();

	int count = 100000000;
	sse_str_t ss = sse_str_new("fifteen");

	int total = 0;
	DWORD tick = GetTickCount();
	for (int i = 0; i < count; ++i) {
		total += toNumber1(ss);
	}
	printf("SSE_SWITCH cost %d %d\n", GetTickCount() - tick, total);

	total = 0;
	tick = GetTickCount();

	for (int i = 0; i < count; ++i) {
		total += toNumber2(ss);
	}

	printf("SSE_SWITCH_SS cost %d %d\n", GetTickCount() - tick, total);
	
	total = 0;
	tick = GetTickCount();
	for (int i = 0; i < count; ++i) {
		total += toNumber(ss);
	}
	printf("IF cost %d  %d\n", GetTickCount() - tick, total);


	sse_str_free(ss);
	return 0;
}


