#pragma once
#include <string.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define SSE_SWITCH_SS(sse_str) switch(sse_str_hash(sse_str,_SSE_SEED_))
#define SSE_SWITCH(str) switch(sse_hash(str,_SSE_SEED_))

#define SSE_CASE(str) case SSE_##str


typedef char* sse_str_t;

sse_str_t sse_str_init_buffer_len(char* buf, size_t buf_len,
	const char* str, size_t str_len);

sse_str_t sse_str_init_buffer(char* buf, size_t buf_len,
	const char* str);

sse_str_t sse_str_new(const char* str);
sse_str_t sse_str_new_len(const char* str,size_t len);

void    sse_str_free(sse_str_t str);

size_t  sse_str_len(sse_str_t str);

unsigned int sse_str_hash(sse_str_t str, unsigned int seed);

unsigned int sse_hash(const char* s, unsigned int hash);

#ifdef  __cplusplus
}
#endif

