solution "sse"
	location ( "build" )
	targetdir("build")
	configurations { "Debug", "Release" }
	defines { "_CRT_SECURE_NO_WARNINGS" }
	characterset ("MBCS")
	
	configuration "Debug"
		defines { "DEBUG" }
		flags { "Symbols", "ExtraWarnings"}

	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize", "ExtraWarnings"}			
	
	
	project "sse_gen"
		kind "ConsoleApp"
		language "C++"
		files { 
			"sse/sse_str.h",
			"sse/sse_str.c",
			"sse_gen.cpp",
		}

	project "test"
		kind "ConsoleApp"
		language "C++"
		files { 
			"sse/sse_str.h",
			"sse/sse_str.c",
			"test.cpp",
		}			


