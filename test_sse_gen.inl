#pragma once
#include "sse/sse_str.h"
/*this file is generated by sse_gen don't edit it*/
enum{
	_SSE_SEED_ = 216613626,
	SSE_eight = 133650171,
	SSE_eighteen = 102845221,
	SSE_eleven = 37994273,
	SSE_fifteen = 225870933,
	SSE_five = 189442250,
	SSE_four = 139992050,
	SSE_fourteen = 185628410,
	SSE_nine = 205217978,
	SSE_nineteen = 56273746,
	SSE_one = 127229698,
	SSE_seven = 37047681,
	SSE_seventeen = 12482513,
	SSE_six = 215636242,
	SSE_sixteen = 9254682,
	SSE_ten = 13869983,
	SSE_thirteen = 106556695,
	SSE_three = 1121786,
	SSE_twelve = 84525977,
	SSE_twenty = 50683041,
	SSE_two = 248756628,
};
